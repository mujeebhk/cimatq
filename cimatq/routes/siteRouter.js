var express = require('express');
var router = express.Router();
const nodemailer = require('nodemailer');
const escape = require("escape-html");
const config = require("../config.js");

var locales = [
  { lang: "en", path : "/"},
  { lang: "es", path : "/es/"}
];


var sitePages = [
  {
    name          : "home",
    relativePath  : "",
    template      : "index.ejs",
    showInNav     : true
  },
  {
    name          : "about",
    relativePath  : "about",
    template      : "about.ejs",
    showInNav     : true
  },
  {
    name          : "projects",
    relativePath  : "projects",
    template      : "projects.ejs",
    showInNav     : true
  },
  {
    name          : "services",
    relativePath  : "services",
    template      : "services.ejs",
    showInNav     : true
  },
  {
    name          : "internal-corrosion",
    relativePath  : "services/internal-corrosion",
    template      : "internal-corrosion.services.ejs",
    showInNav     : false
  },
  {
    name          : "chemical-treatment",
    relativePath  : "services/chemical-treatment",
    template      : "chemical-treatment.services.ejs",
    showInNav     : false
  },
  {
    name          : "simulations-and-flow-assurance",
    relativePath  : "services/simulations-and-flow-assurance",
    template      : "simulations-and-flow-assurance.services.ejs",
    showInNav     : false
  },
  {
    name          : "contact",
    relativePath  : "contact",
    template      : "contact.ejs",
    showInNav     : true
  }

]

var templateBase = "site/";

locales.forEach(function(l){
  var lNavigation = [];
  sitePages.forEach(function(k){
    if(k.showInNav) {
      lNavigation.push({url : l.path + k.relativePath, text : k.name});
    }
    
  });

  sitePages.forEach(function(k){

    var data = {};
    data.locale = l;
    data.locales = locales;
    data.navigation = lNavigation;
    data.currPage = k.name;
    data.URL = config.url;

    var route = l.path + k.relativePath;
    var template = templateBase + k.template;
  

    router.get(route, function(req, res, next) {
      res.render(template, data);
    });


  });
});

// POST route from contact form
router.post('/contactform', function (req, res) {

  const q = req.body;

  let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  

  let bodyHtml = "";

  bodyHtml += "<div style='font-size:14px; color:#333; background:#ddd; font-family:sans-serif;padding:2em;'>"
  bodyHtml += "<table cellpadding='0' cellspacing='0' border='0' style='width:90%; margin:0 auto;background:#fff;border:1px solid #ccc'>";
  bodyHtml += "<tr>";
  bodyHtml += "<td colspan='2' style='font-size:12px;padding:8px;color:#999; border-bottom:1px solid #ccc;background:#f4f4f4'>";
  bodyHtml += "New contact form submission from <strong>"+ip+"</strong>";
  bodyHtml += "</td>";
  bodyHtml += "</tr>";

  bodyHtml += "<tr>";
  bodyHtml += "<td style='padding:16px;color:#aaa;text-align:right;width:20%;border-bottom:1px solid #ccc'>";
  bodyHtml += "Name";
  bodyHtml += "</td>";
  bodyHtml += "<td style='padding:16px;text-align:left;border-bottom:1px solid #ccc'>";
  bodyHtml += escape(q.name);
  bodyHtml += "</td>";
  bodyHtml += "</tr>";

  bodyHtml += "<tr>";
  bodyHtml += "<td style='padding:16px;color:#aaa;text-align:right;width:20%;border-bottom:1px solid #ccc'>";
  bodyHtml += "Email";
  bodyHtml += "</td>";
  bodyHtml += "<td style='padding:16px;text-align:left;border-bottom:1px solid #ccc'>";
  bodyHtml += "<a href='mailto:"+escape(q.email)+"'>"+escape(q.email)+"</a>";
  bodyHtml += "</td>";
  bodyHtml += "</tr>";

  if(typeof q.subject !== 'undefined') {

    bodyHtml += "<tr>";
    bodyHtml += "<td style='padding:16px;color:#aaa;text-align:right;width:20%;border-bottom:1px solid #ccc'>";
    bodyHtml += "Subject";
    bodyHtml += "</td>";
    bodyHtml += "<td style='padding:16px;text-align:left;border-bottom:1px solid #ccc'>";
    bodyHtml += escape(q.subject);
    bodyHtml += "</td>";
    bodyHtml += "</tr>";

  }

  bodyHtml += "<tr>";
  bodyHtml += "<td style='padding:16px;color:#aaa;text-align:right;width:20%;border-bottom:1px solid #ccc'>";
  bodyHtml += "Message";
  bodyHtml += "</td>";
  bodyHtml += "<td style='padding:16px;text-align:left;border-bottom:1px solid #ccc'>";
  bodyHtml += escape(q.message);
  bodyHtml += "</td>";
  bodyHtml += "</tr>";

  bodyHtml += "<tr>";
  bodyHtml += "<td colspan='2' style='text-align:right;font-size:24px; font-weight:bold;padding:18px;color:#aaa; border-bottom:1px solid #ccc;background:#f4f4f4'>";
  bodyHtml += "CIMA-TQ";
  bodyHtml += "</td>";
  bodyHtml += "</tr>";

  bodyHtml += "</table></div>";


  bodyText = `${q.name} <${q.email}> says : ${q.message}`;

  
  let mailOpts, smtpTrans;
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: config.smtp.user,
      pass: config.smtp.pass
    }
  });

  var mailOptions = {
    from: config.contactform.from,
    to: config.contactform.to,
    cc: config.contactform.cc,
    subject: config.contactform.subject,
    text: bodyText,
    html: bodyHtml
  };

  if ((req.hostname === 'cimatq.com' || req.hostname === 'www.cimatq.com') && q.name && q.email && q.message && q.message.length >= 5 && q.subject) {
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log('Error occured while sending email. Error= ' + error);
        res.status(500).send({
          status: "Error"
        });
      } else {
        console.log('Email sent: ' + config.contactform.to + '. The service provider response: ' + info.response);
        res.status(200).send({
          status: "OK"
        });
      }
    });
  }
  
});

module.exports = router;
