$(document).ready(function () {



    try {

        try {
            AOS.init();
        } catch(e1) {
            console.error(e1);

        }
        

        try {
            $('.frames').slick({
                arrows: false,
                dots: true,
                appendDots: ".services-slider .controls"
            });

        } catch(e2) {
            console.error(e2);

        }
        

        $(".lang a").each(function () {
            var x = $(this).attr("href");
            var y = $("nav a.current").attr("href");
            if(typeof y !== 'undefined') {
                y = y.split("/");
                y = y[y.length - 1];
                $(this).attr("href", x + y);
            }
            
        });


        $(".drawer-nav").html($("#navigation ul").html());

        $(".nav-drawer-trigger").click(function (e) {
            $(".drawer").addClass("open");
            $("body").addClass("drawer-open");

        });
        $(".drawer-close").click(function (e) {
            $(".drawer.open").removeClass("open");
            $("body").removeClass("drawer-open");

        });


        window.onscroll = function () { scrollFunction() };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                $(".back-to-top").show();
            } else {
                $(".back-to-top").hide();
            }
        }

        $(".back-to-top").click(function (e) {
            e.preventDefault();
            $('html').animate({
                scrollTop: 0
            }, 300);
        });

        $(".contact select").on("click", function(e){
            $(this).find("option.placeholder").attr("disabled", true);
        });

        $(".share").jsSocials({
            url: "http://www.cimatq.com",
            showLabel: false,
            showCount: false,
            shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "whatsapp"]
        });

        $(".contact-form").submit(function(e){
            e.preventDefault();
            var data = {};

            data.name = $(this).find("[name='name']").val();
            data.email = $(this).find("[name='email']").val();
            data.subject = $(this).find("[name='subject']").val();
            data.message = $(this).find("[name='message']").val();

            var $btn = $(this).find("button");
            var $status = $(this).find(".form-status");

            $.ajax("/contactform",{
                method : "post",
                data : data,
                beforeSend : function() {
                    $btn.attr("disabled", true);
                    $status.html("<span class='info'><i class='fas fa-cog fa-spin'></i> Sending...</span>");
                },

                success : function(res) {
                    if(res.status === "OK") {
                        $status.html("<span class='success'><i class='fas fa-check'></i> Message sent</span>");
                    } else {
                        $status.html("<span class='error'><i class='fas fa-times'></i> An error occured</span>");
                    }
                    

                },
                error : function(e,x,y) {
                    console.log(e);
                    $status.html("<span class='error'><i class='fas fa-times'></i> An error occured</span>");
                },

                complete : function(){
                    $btn.attr("disabled", false);
                }
                
            });
        });


    } catch (ex) {
        console.error(ex);
    }
    $("body > .preloader").remove();
    $("body > *").addClass("loaded");
});
